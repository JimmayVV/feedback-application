option explicit
dim thedate,gdcrep,entryrepid
dim ordernumber,lineitemnum,entryrepname
dim ivcode,productnumber,entryrepmanager,entryrepsite
dim feedback, rep, strmanager,feedbacksent,emplast,empfirst,mgrlast,mgrfirst,mgrID, repTNum
dim escapedname


' TEST FILE - 10/2/2015
'
' The expected format of the CMR list is as follows:
'      0      |         1        |         2          |           3         |      4        |           5           |          6           |       7      |       8        |          9          |        10         |          11
' Operator ID |	Personnel Number | Employee Last Name | Employee First Name | Supervisor ID | Supervisor First Name | Supervisor Last Name | Group Suffix | Personnel Area | Personnel Area Name | Employment Status | OrgUnit/Long Text
'


Sub getname()
dim objInfo,strUser,objSys

set objInfo = CreateObject("ADSystemInfo")
struser = objInfo.username

set objSys = GetObject("LDAP://" & strUser)
document.getElementById("comprep").value = objSys.displayname
document.getElementById("today").value = date
end sub





'Checks if Other was selected from Dropdown
Sub CheckSelect()
ivcode = document.getElementById("typeofissue").value

if ivcode = "OTH - Other" then
ivcode = inputbox("Please enter the reason for feedback","Enter Reason")
else
exit sub
end if
end sub






Sub CheckForm()

thedate = document.getElementById("today").value
gdcrep = document.getElementById("comprep").value
entryrepid = document.getElementById("tnumber").value
ordernumber = document.getElementById("ordernum").value
lineitemnum = document.getElementById("lineitem").value
entryrepname = document.getElementById("orderentryrep").value
productnumber = document.getElementById("productnumber").value
entryrepmanager = document.getElementById("repmanager").value
entryrepsite = document.getElementById("repsite").value
feedback = document.getElementById("feedbackinfo").value
mgrID = document.getElementById("mgrID").value
repTNum = document.getElementById("repTNum").value


if entryrepmanager = "" then
feedbacksent = "No"
elseif entryrepmanager <> "" then
feedbacksent = "Yes"
end if

if ivcode = "" then
msgbox "Please select an IV code from the dropdown list"
exit sub
elseif entryrepid = "" then
msgbox "Please enter the order entry rep's T-Number in the appropriate field"
exit sub
'elseif ordertype = "" then
'msgbox "Please select an order type from the dropdown list"
'exit sub
else 
Call SubmitForm()
end if
end sub


Sub SubmitForm()
dim oCon, strSql

set oCon = CreateObject("adodb.connection")
oCon.Provider = "microsoft.jet.oledb.4.0"

oCon.open "InternalQualityDB\InternalQuality.mdb"
feedback = Replace(feedback, "'","''")
entryrepname = Replace(entryrepname, "'","''")
entryrepmanager = Replace(entryrepmanager,"'","''")
ivcode = Replace(ivcode,"'","''")
productnumber = Replace(productnumber,"'","''")
escapedname = Replace(gdcrep,"'","")

if ordernumber = "" then
ordernumber = "00000000"
elseif lineitemnum = "" then
lineitemnum = "00"
end if

strSQL = "INSERT into tblInternalFeedback(Entry_Date,GDC_Associate,Order_Number,Line_Item,Order_Entry_Rep_ID,Order_Entry_Rep_Name,Order_Entry_Rep_Manager,Order_Entry_Site,IV_Code,Feedback_Info,Feedback_Sent,Product_Number)values('" & _
thedate & "','" & escapedname & "','" & ordernumber & "','" & lineitemnum & "','"& entryrepid & "','" & entryrepname & "','"  & entryrepmanager & "','" & entryrepsite & "','" & ivcode & "','" & feedback & "','" & feedbacksent &  "','" & productnumber & "');"

oCon.execute strSql

oCon.close
set oCon = nothing
if entryrepmanager = "" then

msgbox "Thank you.  This issue has been logged into the database."  &vbcr &vbcr & "Since there was no manager listed, no email has been sent."

document.getElementById("GDCForm").reset
exit sub
else
call sendfeedback()
end if


end sub


Sub sendfeedback()
dim olApp,olMsg
dim gdcrep1,gdcrep2,strEmailbody



gdcrep1 = split(gdcrep,",")
gdcrep2 = gdcrep1(1) & " " & gdcrep1(0)

Set olApp = CreateObject("Outlook.Application")
Set olMsg = olApp.CreateItem(0)

With olMsg
	.To = mgrID & "@deluxe.com"
	'.To = mgrfirst & "." & mgrlast & "@deluxe.com" 
	.CC = repTNum & "@deluxe.com"
	.Subject = "Wholestyle Internal Feedback Regarding:   " & entryRepname
	.Body = "Date:  " & thedate & vbcr & vbcr & _
	                "Order Number: " & ordernumber  & "    Line Item:  " & lineitemnum & vbcr & vbcr & _
			"Error Code: " & ivcode & vbcr & vbcr & _
	                "Feedback:    "  & Feedback &vbcr & vbcr & vbcr & _
		"Thank you and if you think this has been assigned in error please see your Team Leader," & vbcr &vbcr & gdcRep2
end With

olmsg.send

msgbox "Thank you.  This issue has been logged and feedback has been sent."

document.getElementById("gdcform").reset
call getName()
end sub



Sub getRep()
dim xlApp,xlbook,dir,objFSO,objFile,xlWsht,empid
dim site,foundrow,searchrange
dim i,j,k,l,trimrange,c,colMod, tnum
dim splitmang,splitemp

empid = document.getElementById("tnumber").value
UCase(empid)


If empid = "" Then

' msgbox ("Please Enter Employee Info")

Else

set objFSO = CreateObject("Scripting.FileSystemObject")
set objFile = objFSO.GetFile("gdcfeedback-wsp.vbs")

dir =  objFSO.GetParentFolderName(objFile)

set xlApp = CreateObject("Excel.Application")
xlapp.visible = False
set xlbook = xlApp.workbooks.open(dir & "\..\..\CMR List.xlsx",,true)
set xlwsht = xlbook.Sheets("CMR List")


Set i = xlwsht.range("A2")
Set j = xlwsht.range("B65536")

Set searchrange = xlwsht.range(i, j)
'Set searchrange = xlwsht.range("A:B")

xlwsht.Select

set foundrow = searchrange.Find(empid, , , 1)

If foundrow Is Nothing Then
msgbox ("The employee information could not be found")
document.getElementById("tnumber").value = ""
document.getElementById("orderentryrep").value = ""
document.getElementById("repmanager").value = ""
document.getElementById("repsite").value = ""

Else
' The expected format of the CMR list is as follows:
'      0      |         1        |         2          |           3         |      4        |           5           |          6           |       7      |       8        |          9          |        10         |          11
' Operator ID |	Personnel Number | Employee Last Name | Employee First Name | Supervisor ID | Supervisor First Name | Supervisor Last Name | Group Suffix | Personnel Area | Personnel Area Name | Employment Status | OrgUnit/Long Text
'
foundrow.select
colMod = foundrow.column - 1
'msgbox ( colMod )
tnum = foundrow.offset(0,1 - colMod).text
emplast = foundrow.offset(0,2 - colMod).text
empfirst = foundrow.offset(0,3 - colMod).text
mgrID = foundrow.offset(0,4 - colMod).text
mgrfirst = foundrow.offset(0,5 - colMod).text
mgrlast = foundrow.offset(0,6 - colMod).text
site = foundrow.offset(0,9 - colMod).text

emplast = trim(emplast)
empfirst = trim(empfirst)
mgrlast = trim(mgrlast)
mgrfirst = trim(mgrfirst)
site = trim(site)

splitmang = split(mgrfirst," ")
splitemp = split(empfirst," ")
empfirst = splitemp(0)
mgrfirst = splitmang(0)

document.getElementById("orderentryrep").value = empfirst & " " & emplast
document.getElementById("repmanager").value = mgrfirst & " " & mgrlast
document.getElementById("mgrID").value = mgrID
document.getElementById("repTNum").value = tnum
document.getElementById("repsite").value = site

End If

xlapp.quit
set xlapp = nothing

End If

end sub
