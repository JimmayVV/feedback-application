option explicit
dim thedate,gdcrep,entryrepid
dim ordernumber,entryrepname
dim ivcode,entryrepmanager,entryrepsite
dim feedback, rep, strmanager,feedbacksent,emplast,empfirst,mgrlast,mgrfirst,mgrID, repTNum
dim typeofissue





Sub getname()
dim objInfo,strUser,objSys

set objInfo = CreateObject("ADSystemInfo")
struser = objInfo.username

set objSys = GetObject("LDAP://" & strUser)
document.getElementById("comprep").value = objSys.displayname
document.getElementById("today").value = date
end sub

'Checks if Other was selected from Dropdown
Sub CheckSelect()
ivcode = document.getElementById("typeofissue").value

if ivcode = "OTH - Other" then
ivcode = inputbox("Please enter the reason for feedback","Enter Reason")
else
exit sub
end if
end sub






Sub CheckForm()

thedate = document.getElementById("today").value
gdcrep = document.getElementById("comprep").value
entryrepid = document.getElementById("tnumber").value
typeofissue = document.getElementById("typeofissue").value
entryrepname = document.getElementById("orderentryrep").value
entryrepmanager = document.getElementById("repmanager").value
entryrepsite = document.getElementById("repsite").value
feedback = document.getElementById("feedbackinfo").value

if entryrepmanager = "" then
feedbacksent = "No"
elseif entryrepmanager <> "" then
feedbacksent = "Yes"
end if



if ivcode = "" then
msgbox "Please select an IV code from the dropdown list"
exit sub
elseif entryrepid = "" then
msgbox "Please enter the order entry rep's T-Number in the appropriate field"
exit sub
else 
Call SubmitForm()
end if


end sub


Sub SubmitForm()
dim oCon, strSql

set oCon = CreateObject("adodb.connection")
oCon.Provider = "microsoft.jet.oledb.4.0"

oCon.open "IdeaDB\IdeaDB.mdb"
feedback = Replace(feedback, "'","''")
entryrepname = Replace(entryrepname, "'","''")
entryrepmanager = Replace(entryrepmanager,"'","''")
ivcode = Replace(ivcode,"'","''")

if ordernumber = "" then
ordernumber = "00000000"

end if

strSQL = "INSERT into tblInternalFeedback(Entry_Date,GDC_Associate,Order_Number,Order_Entry_Rep_ID,Order_Entry_Rep_Name,Order_Entry_Rep_Manager,Order_Entry_Site,IV_Code,Feedback_Info,Feedback_Sent)values('" & _
thedate & "','" & gdcrep & "','" & ordernumber & "','"& entryrepid & "','" & entryrepname & "','" & entryrepmanager & "','" & entryrepsite & "','" & ivcode & "','" & feedback & "','" & feedbacksent & "');"

oCon.execute strSql

oCon.close
set oCon = nothing
if entryrepmanager = "" then

msgbox "Thank you.  This issue has been logged into the database."  &vbcr &vbcr & "Since there was no manager listed, no email has been sent."

document.getElementById("GDCForm").reset
exit sub
else
call sendfeedback()
end if


end sub


Sub sendfeedback()
dim olApp,olMsg
dim gdcrep1,gdcrep2,strEmailbody



gdcrep1 = split(gdcrep,",")
gdcrep2 = gdcrep1(1) & " " & gdcrep1(0)

Set olApp = CreateObject("Outlook.Application")
Set olMsg = olApp.CreateItem(0)

With olMsg
	.To = mgrID & "@deluxe.com"
	.CC = repTNum & "@deluxe.com"
	'.To = mgrfirst & "." & mgrlast & "@deluxe.com" 
	'.CC = empfirst & "." & emplast & "@deluxe.com"
	.Subject = "Design Services Idea for:   " & entryRepname
	.Body = "Date:  " & thedate & vbcr & vbcr & _
	                "What this will impact:    "  & typeofissue &vbcr & vbcr & vbcr & _
	                "Idea and Implementation recommendation:    "  & Feedback &vbcr & vbcr & vbcr & _
		"A new idea for DS" & vbcr &vbcr & gdcRep2
end With

olmsg.send

msgbox "Thank you.  This issue has been logged and feedback has been sent."

document.getElementById("gdcform").reset
call getName()
end sub



Sub getRep()
dim xlApp,xlbook,dir,objFSO,objFile,xlWsht,empid
dim site,foundrow,searchrange
dim i,j,k,l,trimrange,c,colMod
dim splitmang,splitemp

set objFSO = CreateObject("Scripting.FileSystemObject")
set objFile = objFSO.GetFile("gdcfeedback.vbs")

dir =  objFSO.GetParentFolderName(objFile)

set xlApp = CreateObject("Excel.Application")
xlapp.visible = False
set xlbook = xlApp.workbooks.open(dir & "\..\CMR List.xlsx",,true)
set xlwsht = xlbook.Sheets("CMR List")

empid = document.getElementById("tnumber").value
UCase(empid)

xlwsht.Select

If empid = "" Then
   ' Nothing at the moment, but basically don't even try anything if the field is empty
Else

Set i = xlwsht.range("A1")
Set j = xlwsht.range("B65536")

Set searchrange = xlwsht.range(i, j)

set foundrow = searchrange.Find(empid, , , 1)

If foundrow Is Nothing Then
msgbox ("The employee information could not be found")
document.getElementById("tnumber").value = ""
document.getElementById("orderentryrep").value = ""
document.getElementById("repmanager").value = ""
document.getElementById("repsite").value = ""

Else
' The expected format of the CMR list is as follows:
'      0      |         1        |         2          |           3         |      4        |           5           |          6           |       7      |       8        |          9          |        10         |          11
' Operator ID |	Personnel Number | Employee Last Name | Employee First Name | Supervisor ID | Supervisor First Name | Supervisor Last Name | Group Suffix | Personnel Area | Personnel Area Name | Employment Status | OrgUnit/Long Text
'
foundrow.select
colMod = foundrow.column - 1
'msgbox ( colMod )
repTNum = foundrow.offset(0,1 - colMod).text
emplast = foundrow.offset(0,2 - colMod).text
empfirst = foundrow.offset(0,3 - colMod).text
mgrID = foundrow.offset(0,4 - colMod).text
mgrfirst = foundrow.offset(0,5 - colMod).text
mgrlast = foundrow.offset(0,6 - colMod).text
site = foundrow.offset(0,9 - colMod).text

repTNum = trim(repTNum)
emplast = trim(emplast)
empfirst = trim(empfirst)
mgrID = trim(mgrID)
mgrlast = trim(mgrlast)
mgrfirst = trim(mgrfirst)
site = trim(site)

splitmang = split(mgrfirst," ")
splitemp = split(empfirst," ")
empfirst = splitemp(0)
mgrfirst = splitmang(0)

document.getElementById("orderentryrep").value = empfirst & " " & emplast
document.getElementById("repmanager").value = mgrfirst & " " & mgrlast
document.getElementById("repsite").value = site
document.getElementById("mgrID").value = mgrID
document.getElementById("repTNum").value = repTNum

End If

End If

xlapp.quit
set xlapp = nothing

end sub
